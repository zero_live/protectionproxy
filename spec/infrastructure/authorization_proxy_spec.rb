require_relative '../../system/infrastructure/authorization_proxy'

describe 'AuthorizationProxy' do
  it 'pass the necesary arguments to the action' do
    action_arguments = { something: 'arguments' }
    authorized = true
    action = ActionTest.new
    proxy = AuthorizationProxy.new(action)
    proxy.authorize(authorized)

    proxy.do(action_arguments)

    expect(action.have_been_called_with?(action_arguments)).to be_truthy
  end

  it 'does not executes the action if not authorized' do
    arguments_with_unauthorized_user = { current_user: 'unauthorized@user.com', arguments: 'something'  }
    no_authorization_message = { error: 'Unauthorized User' }
    not_authorized = false
    action = ActionTest.new
    proxy = AuthorizationProxy.new(action)

    proxy.authorize(not_authorized)

    result = proxy.do(arguments_with_unauthorized_user)
    expect(result).to eq(no_authorization_message)
  end

  private

  class ActionTest
    def initialize
      @arguments = nil
    end

    def do(arguments)
      @arguments = arguments

      nil
    end

    def have_been_called_with?(other)
      @arguments == other
    end
  end
end
