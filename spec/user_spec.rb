require 'json'
require 'rack/test'

require_relative '../asesora'

describe 'Users' do

  include Rack::Test::Methods

  def app
    Asesora
  end

  before(:each) do
    @params = { 'current_user' => "authorized@email.com" }
  end

  it 'returns complete authorized users catalog' do

    post '/api/retrieve-authorized-users', @params.to_json

    catalog = JSON.parse(last_response.body)
    expect(catalog['data'].size).to eq(4)
  end

  it 'can add one authorized user' do
    new_user = "new_user@gmail.com"

    post '/api/add-authorized-user', authorize({email: new_user}).to_json

    post '/api/retrieve-authorized-users', @params.to_json
    authorized_users_updated = JSON.parse(last_response.body)["data"]
    expect(authorized_users_updated).to include(new_user)
  end

  it 'can add two authorized users' do
    new_user_one = "new_user_one@gmail.com"
    new_user_two = "new_user_two@gmail.com"

    post '/api/add-authorized-user', authorize({email: new_user_one}).to_json
    post '/api/add-authorized-user', authorize({email: new_user_two}).to_json

    post '/api/retrieve-authorized-users', @params.to_json
    authorized_users = JSON.parse(last_response.body)["data"]
    expect(authorized_users).to include(new_user_one, new_user_two)
  end

  def authorize(params)
    @params.merge(params)
  end
end
