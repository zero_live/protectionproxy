# Protection Proxy Example

Here you can find an example of pattern Protection Proxy that we used at Devscola.

We are using a part of real application and we applied the protection proxy for authorize some endpoints.

Our Proxy is protecting some application actions. We need to create a Builder for serve the actions/proxies to the endpoints.

The proxy file is at `system/infrastructure/authorization_proxy.rb`

## System requirements

- Docker version 18.09.6.
- docker-compose version 1.18.0.

## Execute the example

`docker-compose up --build`

## Run tests

Once you have the example running, you have to execute:

`docker-compose exec api rspec`
