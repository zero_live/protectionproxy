
class AuthorizationProxy
    UNAUTHORIZED_MESSAGE = { error: 'Unauthorized User' }

    def initialize(action)
      @action = action
      @is_autorized = false
    end

    def authorize(is_autorized)
      @is_autorized = is_autorized
    end

    def do(params)
      return UNAUTHORIZED_MESSAGE unless @is_autorized

      @action.do(params)
    end
  end
