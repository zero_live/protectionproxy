module Catalogs
  class Service
    class << self
      def authorized_users
        read("authorized_users.json")
      end

      def add_authorized_user(user)
        new_authorized_users = authorized_users | [user]
        update("authorized_users.json", new_authorized_users)
      end

      private

      def read(name)
        from_path = File.dirname(__FILE__)
        collection_file = File.join(from_path, "collections", name)
        catalog = File.read(collection_file, encoding: 'UTF-8')
        JSON.parse(catalog)
      end

      def update(file, value)
        from_path = File.dirname(__FILE__)
        collection_file = File.join(from_path, "collections", file)
        File.open(collection_file, 'w') do |file|
          file.puts value.to_json
        end
      end
    end
  end
end
