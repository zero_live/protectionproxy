require_relative '../infrastructure/authorization_proxy'
require_relative '../services/catalogs/service'
require_relative 'retrieve_authorized_users'
require_relative 'add_authorized_user'

module Actions
  class Builder
    def initialize()
      @is_authorized = false
    end

    def self.for(user)
      builder = new.for(user)

      builder
    end

    def for(user)
      @is_authorized = ::Catalogs::Service.authorized_users.include?(user)

      self
    end

    def retrieve_authorized_users
      proxy = AuthorizationProxy.new(RetrieveAuthorizedUsers)

      proxy.authorize(@is_authorized)

      proxy
    end

    def add_authorized_user
      proxy = AuthorizationProxy.new(AddAuthorizedUser)

      proxy.authorize(@is_authorized)

      proxy
    end
  end
end
