require_relative '../services/catalogs/service'

module Actions
  class AddAuthorizedUser
    class << self
      def do(email:)
        ::Catalogs::Service::add_authorized_user(email)
      end
    end
  end
end
