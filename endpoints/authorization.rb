require_relative '../system/actions/builder'

module Endpoints
  class Authorization
    def self.define_endpoints(api)
      api.post '/api/retrieve-authorized-users' do
        payload = JSON.parse(request.body.read, {:symbolize_names => true})
        current_user = payload[:current_user]
        params = Authorization.remove_current_user(payload)

        message = Actions::Builder.for(current_user).retrieve_authorized_users.do(params)

        {data: message}.to_json
      end

      api.post '/api/add-authorized-user' do
        payload = JSON.parse(request.body.read, {:symbolize_names => true})
        current_user = payload[:current_user]
        params = Authorization.remove_current_user(payload)

        message = Actions::Builder.for(current_user).add_authorized_user.do(params)

        {data: message}.to_json
      end
    end

    def self.remove_current_user(payload)
      params = payload.dup
      params.delete(:current_user)

      params
    end
  end
end
